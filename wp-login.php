<?php

/**
 * @file
 * This is a minor honeypot for crawlers looking for wp-login.php endpoints.
 *
 * This script sleeps for 28 seconds then returns a WSOD.
 */

header("Content-Type: text/plain; charset=utf-8");
sleep(28);
