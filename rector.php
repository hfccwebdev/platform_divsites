<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\CodeQuality\Rector\Ternary\SwitchNegatedTernaryRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;

return static function (RectorConfig $rectorConfig): void {
  $rectorConfig->fileExtensions(['php', 'module', 'theme', 'install', 'profile', 'inc', 'engine']);

  // Define sets of rules.
  $rectorConfig->sets([
    SetList::CODE_QUALITY,
    LevelSetList::UP_TO_PHP_74
  ]);

  // Register a single rule.
  $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

  // Skip rules.
  $rectorConfig->skip([
    SwitchNegatedTernaryRector::class,
  ]);

};
